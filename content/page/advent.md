---
title: Advent
subtitle: Devotional Resources for the Advent Season
comments: false
---

## What is Advent?
The celebration of the coming of Christ

## Advent Devotional Resources

 - [The Dawning of Indestructable Joy, by John Piper](/files/the-dawning-of-indestructible-joy-en.pdf)
 - [Advent Devotional Guide, by The Village Church](/files/tvc-advent-guide.pdf)

