---
title: Homeschool Mission
subtitle: Purpose and Goals of Schooling Children at Home
comments: false
---

We've decided to homeschool our children (for now).  This page is meant to be an explanation of how we arrived at the conclusion that homeschooling was the best choice for us.  Additionally, the following sections will enumerate our goals in homeschooling.

## Big Picture

It's our job as parents to teach and instruct our children (Deuteronomy 6:7).  As such, for our family, the best mechanism for that is through education at home, with their mother.

## Purpose of Education

 - To endow our children with the proper tools for the understanding and enjoyment of God and the world around them.  This includes the ability to read God's word, understand and formulate ideas, think critically about claims preented, and lay the foundation for them to have a means for providing for their faimilies.See the [Old Deluder Satan Act](../old-deluder-satan-act) as an example of this sentiment.  // TODO finish decomposing this into indvidual ideas
 - // TODO to engate in discourse
 - To lay a piece of the foundation for our children's means of providing for themselves and their families.  Since we believe that work - namely work that makes uses of the gifts God has given to produce something useful for society and earn a living - is an essential part of life ([Ecc. 3:12-13](https://www.esv.org/Ecclesiastes+3+12/)), we want to give them the educational tools to help build a skill set that will enable good and valuable labor.

## Relevant Scriptures by Topic
Non-exhaustive list of scriptures I frequented when working through the decision on how to best school our children.

#### It Is a Duty of Parents to Teach Children About God
Instructions are plainly given to parents to educate their children about God (YWHW, one true God of the Bible).  As such, the bible seems to indicate the responsibility of teaching falls _primarily_ on the parents or those rearing children.  Like many other imperatives in scripture, this defines the **what**, but it's up to the parents to decide on the **how**.

**Deuteronomy 4:9-10**

> _Only take care, and keep your soul diligently, lest you forget the things that your eyes have seen, and lest they depart from your heart all the days of your life. **Make them known to your children and your children's children—**  how on the day that you stood before the LORD your God at Horeb, the LORD said to me, ‘Gather the people to me, that I may let them hear my words, so that they may learn to fear me all the days that they live on the earth, and that they may teach their children so.’_ 

**Deuteronomy 6:4-8** (Also restated in **Deuteronomy 11**)

> _Hear, O Israel: The LORD our God, the LORD is one.  You shall love the LORD your God with all your heart and with all your soul and with all your might.  And these words that I command you today shall be on your heart.  **You shall teach them diligently to your children**, and shall talk of them when you sit in your house, and when you walk by the way, and when you lie down, and when you rise.  You shall bind them as a sign on your hand, and they shall be as frontlets between your eyes.  You shall write them on the doorposts of your house and on your gates._ 

**Psalm 78:1-4**

> _Give ear, O my people, to my teaching;  
incline your ears to the words of my mouth!  
I will open my mouth in a parable;  
I will utter dark sayings from of old,  
things that we have heard and known,  
that our fathers have told us.  
**We will not hide them from their children,  
but tell to the coming generation**  
the glorious deeds of the LORD, and his might,  
and the wonders that he has done._  

**Ephesians 6:1-4**

> _Children, obey your parents in the Lord, for this is right.  “Honor your father and mother” (this is the first commandment with a promise),  “that it may go well with you and that you may live long in the land.”  Fathers, do not provoke your children to anger, but **bring them up in the discipline and instruction of the Lord**._ 

#### True Knowledge Starts with God

// TODO

## What Homeschooling is __Not__
Just as there are good reasons to homeschool children, so to there are bad ones.  Here a few common reasons we've encountered for homeschooling that our family denies.

#### A Way To Shelter Our Kids

// TODO You can't protect them, only make them resilient

#### Salvation from Indoctrination of Worldly Ideology
There is a common narrative that goes something like this: Schools nowadays are filled with prayerless, Godless, pagans who indoctrinate your children with secularized teachings as peers, teachers, and administrators.  To turn your kids over to said pagans for 30+ hours a week is to abdicate your duty as a parent and to surrender their minds to the inundation of a secular and Godless worldview.

This view is espoused by many prominent teachers in protestant circles.  For example, here's a quote from Voddie Baucham Jr's _Family Driven Faith_ (pp 126):

> _Clearly, believers are to avoid unnecessary exposure to worldview influences that would contradict and/or undermine biblical truth.  Again, any educational choice we make must take this biblical principal into account._

Baucham goes on to make his case for the abandonment of public schools, saying (pp 128):

> _Do everything in your power to avoid the influence of government schools that are incapable of bringing our children up in "the discipline and instruction of the Lord" (Ephesians 6:4)_

Here's the primary issue I have with this thesis: interactions with individuals of different worldviews does not **necessarily** lead to indoctrination.  Let me break this argument down into its core propositions.  I'm doing my best not to create a flimsy straw man here, but to accurately state the arguments.  Here they are:

 1. As parents, we are given the task of rearing our children in the teachings and instruction of the Lord (see above ^ scriptures)
 1. Public schools are filled with people espousing a non-Christian worldview in their thoughts, speech, and deeds
 1. Willing exposure to non-Christian worldviews, particularly in daily relationships like a school setting, **necessarily** undermines Christian teaching and training
 
Therefore, according to this argument, to send our kids to public schools, is to willingly undermine the teaching and training we're responsbile for biblically as parents.

Obviously, our family has no qualms with propositions #1 or #2 above.  However, we take strong issue with the broad application of proposition #3.  Here's why:

 - More time != greater influence - While much time is spent in school, there is also a great deal of time spent outside of school.  Furthermore, I'd argue that while kids may spend more _time_ attending classes, parents can certainly gain more focus, trust, and attentiveness through intentionality as well as the nature of parental bonds.  These elements, when applied appropriately, can certainly allow for parents to wield a far stronger influence in their child's life than any institution.  The converse of this principal is also true (and should be a warning to lazy homeschoolers), in that having more time with children during homeschool will not _necessarily_ lead to greater influence in their lives.
 - Exposure != indoctrination - Much of this argument lies on the necessary link between attending a public school and indoctrination into a secular or humanist worldview.  While I will readily admit that schools are used by those in authority to not only communicate information, but also a means of pushing for a homogeny of thought around particular worldview ideals, I also hold to the notion that this practice is not entirely effective.  Complete homogeny of thought - particularly through educational means (as opposed to opression) has, to my knowledge, never come close to being accomplished.  Therefore, if indoctrination to said worldview is not a *necessary* consequence of attending public institutions, believers are free to weight the consequences of institutional learning around a number of concerns (I.E. slant of the school system, child's temperament, age, and parent's availability).  Once again, I'm not arguing that the influence of public eductors and administrators will have no effect.  However, I *am* arguring that the effects are not *necessarily* and *completely* decided as influences that will lead to children's indoctrination into an non-biblical workdlview.

Reason why we reject:
 - Abstaining from sinful activities **solely** from lack of choice/opporunity does not imply an obedient heart or effectively teach true obedience.  