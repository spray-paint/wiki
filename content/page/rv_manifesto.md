---
title: RV Manifesto
subtitle: It's going to be tough.  Let's remember why.
date: "2019-10-22"
comments: false
---

When going on an RV trip, it's important to set our expectations appropriately.  In order to prepare our hearts and minds, let's read through our shared expectations so we know what we're getting ourselves into:

 - **We probably won't get there on time.**  Whether it's to the original destination, attractions, or just trying to get the laundry done at the campsite.  RV'ing with small kids means that we won't be able to keep tight schedules or plan much more than an hour or two in advance with any degree of certainty.  We'll let go of our schedules joyfully because the trips are _about spending time together, rather than being able to stick to a schedule_.
 - **Things will break.**  Expect the car, the camper, and everything in them to break or otherwise not function properly when we need them.  They will cost money to fix - probably more than we expected.  We embrace that our stuff breaks because _we are choosing to spend our things to grow our family's world through travel and exploration_.  
 - **We'll have to spend money unexpectedly.**  While we will seek to steward and set aside money as best we can, unexpected expenses will arise on these trips.  We will have to get last minute supplies (perhaps some that we forgot at home), get hit with unexpected fees, or have to pay for services we don't have time to complete ourselves.  We willingly accept the expenses we will incur _because the respite from daily life and adventures in our God's creation serves a kingdom purpose that money alone does not_.  What better end could our money serve?
 - **The kids will be ungrateful.**  The time, effort, and money we spend will be lost on the children.  They will whine, and ask for the simplest forms of pleasure (like movies or cookies) when deep and meaningful experiences are set before them.  We do not expect them to understand, but instead _commit to pouring into our children through RV'ing_.
 - **There will be poop.**  Bodily functions, sleep schedules, exercise, diet, and all the staples of everyday life will be affected.  We embrace the disruptions, knowing that _they are only for a brief season, one which usually ends too quickly_.
 - **The weather may not cooperate.**  We will have rain, wind, heat, cold, and storms - often times when it is least convenient.  The weather will make us cancel exciting excursions and impose constraints on us.  We will remember that _the weather is under the soveriegn control of a good God, and accept His providence with thanksgiving_.
 - **We're going to fight.**  RV life is hard, and the stresses will inevitably surface in the form of marital tension.  When we disagree, fight, and otherwise say things we don't mean, we must be quick to repent and eager to forgive.  _We must emabrace the selfless, Christ-like love of one another if we're going to grow our marriage through RV'ing_.

Read these before or during every trip, and pray that our God would use these times to help our family to love Him more!
