---
title: Old Deluder Satan Act
subtitle: Written by the Massachusettes Bay Colony in 1647
comments: false
---

Passed by the Massachusettes Bay Colony in 1647, this act established a requirement for townships of 50 or more households to establish a school for the purpose of teaching children to read and write **that they might not be deceived by Satan**.

## Full Text

It being one chief project of that old deluder, Satan, to keep men from the knowledge of the Scriptures, as in former times by keeping them in an unknown tongue, so in these latter times by persuading from the use of tongues, that so that at least the true sense and meaning of the original might be clouded and corrupted with love and false glosses of saint-seeming deceivers; and to the end that learning may not be buried in the grave of our forefathers, in church and commonwealth, the Lord assisting our endeavors.

It is therefore ordered that every township in this jurisdiction, after the Lord hath increased them to fifty households shall forthwith appoint one within their town to teach all such children as shall resort to him to write and read, whose wages shall be paid either by the parents or masters of such children, or by the inhabitants in general, by way of supply, as the major part of those that order the prudentials of the town shall appoint; provided those that send their children be not oppressed by paying much more than they can have them taught for in other towns.

And it is further ordered, that when any town shall increase to the number of one hundred families or householders, they shall set up a grammar school, the master thereof being able to instruct youth so far as they may be fitted for the university, provided that if any town neglect the performance hereof above one year that every such town shall pay 5 pounds to the next school till they shall perform this order.