---
title: Thanksgiving
subtitle: Devotional Resources for Celebrating Thanksgiving
comments: false
---

## Resources

 - [Lincoln's Thanksgiving Day Proclamation](/page/lincoln-thanksgiving-proclamation)

## Devotional Thoughts
Below is an assortment of thoughts on passages that reflect an attitude of thankfulness towards Christ.  This collection is certainly not meant to be either systematic or exhaustive.  Instead, it's a manifold gathering of thoughts I've been encouraged by and found useful for sharing with my family and friends.

#### Romans 1:21 - Lack of Thanksgiving as Rebellion
In Romans chapter 1, Paul is walking through the active rejection God by non-jews.  In verse 21, he states that:

> For although they knew God, they did not honor him as God or give thanks to him, but they became futile in their thinking, and their foolish hearts were darkened.

Generally, the focus on the verse falls on the idea that they rebel by not submitting (honoring).  However, this voice also clearly points out that a right orientation towards our creator and sustainer is thankfulness.  And to have an attitue otherwise is actually an act of rebellion.

#### I Chronicles 29 - Thankfulness in Temple Frewill offerings
During this time of transition from David -> Solomon, David begins setting aside treasure for the building of the temple.  He moves on to call the people of Israel to make freewill offerings just as he did, then offers a prayer of thanks to God.  His prayer is a beautiful reflection of the purpose of God's blessing, and how we might rightfully reflect on the proper use of His gifts.
