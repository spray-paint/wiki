---
title: Family Memory Verses
subtitle: Manifest of Various Scriptures our Family has Memorized
comments: false
---

**John 3:16**

> For God so loved the world, that he gave his only Son, that whoever believes in him should not perish but have eternal life.

**Matthew 7:12**

> So whatever you wish that others would do to you, do also to them, for this is the Law and the Prophets.

**Psalm 119:105**

> Your word is a lamp unto my feet and a light to my path

**Ephesians 6:1-2**

> Children, obey your parents in the Lord, for this is right.  “Honor your father and mother” (this is the first commandment with a promise)

**I Corinthians 1:18**

>  For the word of the cross is folly to those who are perishing, but to us who are being saved it is the power of God. 

**Joshua 1:9**

> Have I not commanded you? Be strong and courageous. Do not be frightened, and do not be dismayed, for the LORD your God is with you wherever you go.”

**Psalm 23**

>The LORD is my shepherd; I shall not want.
>  He makes me lie down in green pastures.
>  He leads me beside still waters.
>  He restores my soul.
>  He leads me in paths of righteousness
>  for his name's sake.
>
>Even though I walk through the valley of the shadow of death,
>  I will fear no evil,
>  for you are with me;
>  your rod and your staff,
>  they comfort me.
>
>You prepare a table before me
>  in the presence of my enemies;
>  you anoint my head with oil;
>  my cup overflows.
>
>Surely goodness and mercy shall follow me
>  all the days of my life,
>  and I shall dwell in the house of the LORD forever.