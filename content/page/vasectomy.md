---
title: Permanent Birth Control
subtitle: Biblical, or not?
date: "2019-11-06"
comments: false
---

My family is approaching the birth of our (*n*th) child.  We knew we wanted a larger family, but until now have not answered the question of "how large?"  This post offers some manifold thoughts on our decision making process - particularly as it pertains to permanent birth control matters.

## Not This

This post is *NOT* the sad rationale I've heard many men make in my community of faith.  When I ask about "What are your thoughts on whether or not a vasectomy is biblical and right?"  I get answers like this "many men of the faith that I respect have done it, so I trust that it's fine."  I've been floored by the number of men around me that give this answer.  Make no mistake... this answer is effectively outsources the operation of your conscience to that witch 'respectful' men have done.  While it's not harmful to look at other men of the faith in your decision-making process, this action alone is not enough.  How does one relate to God and engage in intimacy with one's spouse as an act of worship, having taken permanent steps, if we do not know the path that connects our actions to the Kingdom of God?  How can we rightfully say that some action is God-glorifying and good, according to the standards of the bible, if our only frame of reference is other men?  What an empty rationale - devoid of the interactions with and worship of the God we claim to love.

## But This

This post *IS* an attempt to look at the subject biblically, understand the arguments from various sides, and weight the evidence accordingly.  What's more, this is a public record of a personal process/journey, so a warning to the reader (if any are in fact out there!): The following is not optimized for an audience.

### The Case Against (Permanent) Birth Control

Spoiler alert: I didn't land on this conclusion.  However, I will make every attempt to do justice to the well-formed arguments by intelligent men and women.  It is not my aim to make a straw man out of their position.

#### The Story of Onan in Genesis 38

This story is often quoted as a case study, and the argument goes something like this: "Onan exercised a form of birth control, and was put to death for disobedience.  Therefore, God classifies birth control as disobedience."  First of all, let's look at the text from Genesis 38:8-10:

> Then Judah said to Onan, “Go in to your brother’s wife and perform the duty of a brother-in-law to her, and raise up offspring for your brother.” But Onan knew that the offspring would not be his. So whenever he went in to his brother’s wife he would waste the semen on the ground, so as not to give offspring to his brother. And what he did was wicked in the sight of the Lord, and he put him to death also.

Notice what's apparent in the text: Onan has a mandate to perform his duties as a brother-in-law and raise up offspring in the line of his brother.  Onan rebels against this mandate and is subsequently put to death.  However, I would agree with Douglas Wilson's analysis in that the action of birth control in this story is merely an instrument of rebellion:


> The Scriptures do contain one example of birth control (the example of Onan), but this narrative is complicated by the fact that the whole point of a Levirate marriage was the preservation of a deceased brother’s name through a legitimate heir. Onan therefore defrauded his sister-in-law through the instrumentality of an act of birth control, which makes it problematic to say that he was struck down by the Lord for the birth control simpliciter.

In light of this, I will not address further arguments from Genesis 28.  Many church fathers, from Jerome to Calvin, have made a case for the essential nature of procreative purpose in sexual relations.  It would be redundant to re-state this argument in light of the various commentaries on this passage.


#### Children are a Blessing From the LORD, Psalm 127

Another argument comes from Psalm 127, on the blessings of Children.  Let's start with a look at the passage (v. 3-5):

> Behold, children are a heritage from the Lord,
>   the fruit of the womb a reward.
> Like arrows in the hand of a warrior
>   are the children of one’s youth.
> Blessed is the man
>   who fills his quiver with them!
> He shall not be put to shame
>   when he speaks with his enemies in the gate.

The argument goes something like this: "Since children are a deemed by God as a 'blessing', who are you to stand in the way of God's blessing?  By temporarily or permanently halting children, you are actively rejecting the blessings of God."  The argument is often accompanied with questions of "Do you not believe that God is sovereign over the opening and closings of wombs?  Will He not bring blessings appropriately in His time?"

I see two main problems with this argument.  The first problem is that we have a faulty line of reasoning: if something is a 'blessing' from God, it is not automatically implied that we should posture ourselves to receive the highest quantity possible of that blessing.  For instance, food is a blessing from God.  Does that mean that I should consume as much food as possible?  Or that if I were to stop eating, that I'd be rejecting the blessings of God?  This line of reasoning is clearly wanting when it is applied to other areas of the Christian life.

Secondly, children are to be understood as a covenant blessing.  I'll once again lean on Douglas Wilson for an explanation here:

> Children are a blessing from the Lord, but not an automatic blessing from the Lord. They are a covenant blessing, which means the terms of the covenant must always be believed and remembered. A man with a lazy son is ashamed (Prov. 10:5), and it should be obvious that he would not be more greatly blessed if he had five sons sleeping through the harvest.

#### Voddie Baucham Opposing (Permanent) Birth Control

Voddie Baucham would strongly advocate against steps like a vasectomy.  Here are a few quotes and rebuttles, that may shed a bit more light on this side's arguments.


> When Trey was born, we hired a doctor to speak to God on our behalf. He took his scalpel and sutures and told God, 'The Bauchums hereby declare that they no longer trust, nor welcome you in this area of their lives.'

> Here we are, God. We know we are "fearfully and wonderfully made", but we don't like it anymore. We have hired this Dr. to cut, shorten, tie and cauterize because we want to be sure you don't pull any tricks. It is time to have fun without the responsibility--it is all about US now. We know you would take care of our family if you gave us more children, but we don't want to give up our name brand clothes and fast food meals. Yes, we are grateful for the two you have given us, but not enough to want more. No thanks!!

Notice the (false) dichotomy Baucham introduces: Either you should have as many kids as possible, or you love name brand clothes.  What of abstaining for the sake of Kingdom purposes?  Say you're about to be highly mobile on the mission field, or your wife is struggling to perform her duties as a mother to multiple existing kids?  Are those not sufficient purposes?  Where is their place here?  It's not even registered on the dichotomy he puts forth.

To be fair, Baucham does (eventually) put some qualifiers around his statements:

> Before you throw this book down (or have a heart attack), I am not suggesting thata everyone has to have seven children.  I think there are legitimate reasons to limit family size.  However, I have onnly met a handful of people whose family size was limited for any of those legitimate reasons.  I usually meet people who stopped haaving kids because they got their boy and their girl, so they're "the perfect 'little' family."  Or I meet people who have calculated (and extrapolated) the cost of their children's college education, their annual vacation, and their early retirement and determined that 1.9 children is their break-even point.  I also meet people whose children are undisciplined, untrained, and out of control, so they find it too stressful to have more kids.  Rare is the couple who left the doctor's office with a legitimate warning against further pregnancies.

At best, Voddie has strayed into a light neonomianism on children in an attempt to right the ship against the pull of the culture's anti-kid philosophy.  At worst, he's made family size an idol.

#### Church Fathers on Birth Control

The following is an attempt to examine some thoughts from early church fathers on birth control.  There are some additional, Roman Catholic-leaning excerpts [here](https://www.churchfathers.org/contraception).

##### Chrysostom

One of Chrysostom's strongest critiques comes from a discussion of Matthew 8:28, in his work "Homilies on Matthew."  Chrysostom discusses the demon-posessed men of the Gadarenes, saying that "madness" demonstrated by (then) modern men could be likened to the behavior of the demoniac:

> For so when any man is dissolute, eager after all embraces, he differs not at all from the demoniac, but goes about naked like him, clad indeed in garments, but deprived of the true covering, and stripped of his proper glory; cutting himself not with stones, but with sins more hurtful than many stones. Who then shall be able to bind such a one? Who, to stay his unseemliness and frenzy, his way of never coming to himself, but forever haunting the tombs? For such are the resorts of the harlots, full of much evil savor, of much rottenness.

Chrysostom goes on to liken the unbound, demon-posession of Matthew 8 to the "covetous man":

> Yet is the covetous man much more fierce even than this, assailing all like hell, swallowing all up, going about a common enemy to the race of men. Why, he would have no man exist, that he may possess all things. And he stops not even at this, but when in his longing he shall have destroyed all men, he longs also to mar the substance of the earth, and to see it all become gold; nay, not the earth only, but hills also, and woods, and fountains, and in a word all things that appear.

As this covetous man seeks to destroy all relations that might threaten his status and autonomy, children are then included as targets in the line of fire for his covetousness:

> and that which is sweet, and universally desirable, the having children, they esteem grievous and unwelcome: many at least with this view have even paid money to be childless, and have maimed their nature, not only by slaying their children after birth, but by not suffering them even to be born at all.

So to summarize Chrysostom's argument from Matthew 8:

 - This picture of demon possession, or "madness" is one of self-harm in rebellion against the proper order of the creator, and can take many other forms than physical isolation and harm: "For so when any man is dissolute, eager after all embraces, he differs not at all from the demoniac, but goes about naked like him, clad indeed in garments, but deprived of the true covering, and stripped of his proper glory; cutting himself not with stones, but with sins more hurtful than many stones."
 - A primary manifestation of this form of possession of covetousness, as it will be largely "unbound."
 - One manifestation of covetousness is the evil hostility towards children, as manifested in their slaying after birth, or mutilations through which further children might not be had.

Essential to the claim laid out by Chrysostom is this: that any "mutiliations" which prevent further children are derived from the sin of covetousness.  However, as will be stated in the upcoming case for the permissability of this action, I would contest that in a fallen world, there might be good and right reasons to stop having children.  It is an entirely consistent position to say "We think children are a blessing, and think well of them, yet we think we have reached sufficiently many given our context and frame."

Finally, I'd also remark that the biblical basis for Chrysostom's claim comes from extrapolating principles from a text dealing primarily with the irratonialities and manifest madness of being given to indwelling sin, Christ's power to overcome, and the restoration that comes from living in right relationship with God.  The cultural application of these principles is subject to the exegete's context, and may be more quickly rejected than the interpretation of the passage itself.

[Chrysostom's Homily on Matthew 8](https://www.newadvent.org/fathers/200128.htm)

##### Augustine

Augustine is often quoted by Roman Catholics in an attempt to paint non-procreative intimacy as something inherently sinful and new in church life.  His work "On Marriage and Concupiscence" is often used (Concupiscence = lust/sexual desire).  For instance, Chapter 17 of this work reads:

> It is, however, one thing for married persons to have intercourse only for the wish to beget children, which is not sinful: it is another thing for them to desire carnal pleasure in cohabitation, but with the spouse only, which involves venial sin. For although propagation of offspring is not the motive of the intercourse, there is still no attempt to prevent such propagation, either by wrong desire or evil appliance. They who resort to these, although called by the name of spouses, are really not such; they retain no vestige of true matrimony, but pretend the honourable designation as a cloak for criminal conduct. Having also proceeded so far, they are betrayed into exposing their children, which are born against their will. They hate to nourish and retain those whom they were afraid they would beget. This infliction of cruelty on their offspring so reluctantly begotten, unmasks the sin which they had practised in darkness, and drags it clearly into the light of day. The open cruelty reproves the concealed sin. Sometimes, indeed, this lustful cruelty, or, if you please, cruel lust, resorts to such extravagant methods as to use poisonous drugs to secure barrenness; or else, if unsuccessful in this, to destroy the conceived seed by some means previous to birth, preferring that its offspring should rather perish than receive vitality; or if it was advancing to life within the womb, should be slain before it was born. Well, if both parties alike are so flagitious, they are not husband and wife; and if such were their character from the beginning, they have not come together by wedlock but by debauchery. But if the two are not alike in such sin, I boldly declare either that the woman is, so to say, the husband's harlot; or the man the wife's adulterer.

Fairly stong statements, seemingly against any form of concupiscence, or marital relations NOT involving the possibility for procreation.  However, I would point out a few observations:

 - This doctrine stems from the idea that non-procreative sex is venial sin, and (as we'll see in the subsequent quote), only made permissible by marriage.  If you hold to the truth that sexual expression within marriage is not inherently sinful (see Song of Solomon), this line of reasoning begins to lose its weight.
 - There is likely a strong influence from Stoicism on Augustine here.  Just as contemporary men might be influenced to have an outsized view of the role of sex or overemphasize the pursuit of physical pleasure, so too the Stoics' tendency to render desire as sin seems to be a cultural force that made lasting impressions on the doctrinal foundations of the early church. 
 - Notice that the harsh words come to those wishing to prevent offspring by "wrong desire or evil appliance"*.  That is those who actively seek to remove the one component of this marital union whereby an otherwise wholly sinful practice is made clean.  This is analogous to the act of removing the filter system from your water source - the entirety of the system is now unclean because the one mechanism that brings purification is removed.  However, this assertion stands or falls based on the premise that non-procreative sex within mariage is unclean.  If the water coming into your system is already clean, then the removal of the filter has no net effect.

> *Fun Fact: there were several herbs used for contraceptive purposes in the ancient world (with limited efficacy).  For instance, Silphium was at one time used for this purpose, and worth more than its weight in silver.  But the most effective means of contraceptive continued to be "coitus interruptus"

Augustine spoke specifically to the (light) permissiveness of non-procreative marital initimacy in the previous chapter, entitled "A Certain Degree of Intemperance is to Be Tolerated in the Case of Married Persons; The Use of Matrimony for the Mere Pleasure of Lust is Not Without Sin, But Because of the Nuptial Relation the Sin is Venial."

> But in the married, as these things are desirable and praiseworthy, so the others are to be tolerated, that no lapse occur into damnable sins; that is, into fornications and adulteries. To escape this evil, even such embraces of husband and wife as have not procreation for their object, but serve an overbearing concupiscence, are permitted, so far as to be within range of forgiveness, though not prescribed by way of commandment: 1 Corinthians 7:6 and the married pair are enjoined not to defraud one the other, lest Satan should tempt them by reason of their incontinence. 1 Corinthians 7:5 For thus says the Scripture: "Let the husband render unto the wife her due: and likewise also the wife unto the husband. The wife has not power of her own body, but the husband: and likewise also the husband has not power of his own body, but the wife. Defraud not one the other; except it be with consent for a time, that you may have leisure for prayer; and then come together again, that Satan tempt you not for your incontinency. But I speak this by permission, and not of commandment." 1 Corinthians 7:3-6 Now in a case where permission must be given, it cannot by any means be contended that there is not some amount of sin. Since, however, the cohabitation for the purpose of procreating children, which must be admitted to be the proper end of marriage, is not sinful, what is it which the apostle allows to be permissible, but that married persons, when they have not the gift of continence, may require one from the other the due of the flesh — and that not from a wish for procreation, but for the pleasure of concupiscence? This gratification incurs not the imputation of guilt on account of marriage, but receives permission on account of marriage. This, therefore, must be reckoned among the praises of matrimony; that, on its own account, it makes pardonable that which does not essentially appertain to itself. For the nuptial embrace, which subserves the demands of concupiscence, is so effected as not to impede the child-bearing, which is the end and aim of marriage.

1 Corinthians 7 leaves Augustine with no choice but to make a similar concession that there is a place for "toleration" of the embraces of husband and wife without procreation as the aim.  But how might he reconcile this clear instruction by Paul with his views on the inherent uncleannes of non-procreative physical intimacy?  The answer comes in the form of venial sin in that the union specifically "makes pardonable" this act.

Here protestant readers must take pause.  We have not categories of sin as Roman Catholics do (mortal, venial).  We do not have a category for acts that are pardonable through ordinary means of penance vs. are not.  Either our actions are in line will the will and design of our God and should be embraced, or they are not and should be rejected.  This means that the acceptance of non-procreative sex as a venial sin is not acceptable for our purposes.

Finally, I would like to point out that "On Marriage and Concupiscence" was written specifically to refute Pelagianism, a heresy in the early church that taught that original sin was not passed on, that babies were not inherently tainted by the fall of Adam.  Augstine closes with these remarks:

> In respect, however, to this concupiscence of the flesh, we have striven in this lengthy discussion to distinguish it accurately from the goods of marriage. This we have done on account of our modern heretics, who cavil whenever concupiscence is censured, as if it involved a censure of marriage. Their object is to praise concupiscence as a natural good, that so they may defend their own baneful dogma, which asserts that those who are born by its means do not contract original sin.

Augustine asserted that the inherent sinfulness of man (at birth) was due to the inherently sinful act of concupiscence, even within a Christian marriage and with procreation as an accompanying purpose (see chapter 27).  In making the case against Pelagianism, this is the cornerstone of his argument.  Remove the inherent sinfulness of concupiscence, and the house falls.

However, given the idea of federal headship from Romans 5, Protestants do not rely on an inherent sinfulness of marital concupiscence to make a case for the transferrence of original sin.  As such, this idea has been interpreted less as the cornerstone for the case for man's sinfulness, and moreso on its own merits.

[Full Text of Augustine's "On Marriage and Concupiscence"](https://www.newadvent.org/fathers/1507.htm)

Augustine again visits this theme in his work [The Good of Marriage](https://www.newadvent.org/fathers/1309.htm):

> For necessary sexual intercourse for begetting is free from blame, and itself is alone worthy of marriage. But that which goes beyond this necessity, no longer follows reason, but lust. And yet it pertains to the character of marriage, not to exact this, but to yield it to the partner, lest by fornication the other sin damnably. But, if both are set under such lust, they do what is plainly not matter of marriage. However, if in their intercourse they love what is honest more than what is dishonest, that is, what is matter of marriage more than what is not matter of marriage, this is allowed to them on the authority of the Apostle as matter of pardon

Once again, we see Augustine views sexual realtions for anything other than procreative purposes "not matter of marriage", and not begetting the Apostolic allowance as a matter of pardon.

##### Against the Gnostics & Manichees

One of the major influences of early church fathers was the plethora of dualistic heresies, both in the "Christian" and non-Christian variety.  [Gnostics](https://en.wikipedia.org/wiki/Gnosticism), [Manichaeism](https://en.wikipedia.org/wiki/Manichaeism), and [Catharism](https://en.wikipedia.org/wiki/Catharism) all taught some form of elevation of a metaphysical realm/mode of being, and pitted that against the physical.  It was common amongst these forms of religions to think of the physical as common, or even dirty, as they favored some form of metaphysical knowledge or being.

In light of the perceived dirtiness of the physical world, these sects cast a disparaging view of children, as it was seen as a dirty act involving the proliferation of this physical, tainted state.  Procreation was sinful, and it was common in these sects to engage in various forms of brith control.

I'm no history expert, but it appears that many of the church fathers have this sort of worldview in mind when they speak against birth control, as the Gnostics and Manichees were the main types of persons that openly practiced birth control.  If the worldview espoused by Gnosticism was essentially linked to birth control in the minds of the church fathers, then we might have a significant distinction between what they're speaking against and what is being proposed by modern Christians who permanently stop having children after a particular number.  Of course, it's always possible that it could be argued that "Christians" who do such things are, in fact, taking on a form of these heresies, but it would be a stretch to try to press something like Gnosticism on all who claim Christ and prractice birth control.

What appears evident to me is that this heresy is viewed as the primary reason for which one might attempt to exercise birth control.  Much of the early church father's writing on the subject is in response to such groups, and I have yet to find anything addressed to Christians on this question outside of a view on these heresies.

More in response to this view directly in Augustine's [On the Morals of the Manichees](https://www.newadvent.org/fathers/1402.htm), as he is particularly qualified to shed light on this, having spent time in his youth as a Manichean.

#### Reformers on Birth Control

Much of the critique of birth control comes from the mandate to multiply in Genesis 2, and Onan's sin in Genesis 38.  Calvin had this to say:

> I will contend myself with briefly mentioning this, as far as the sense of shame allows to discuss it. It is a horrible thing to pour out seed besides the intercourse of man and woman. Deliberately avoiding the intercourse, so that the seed drops on the ground, is doubly horrible. For this means that one quenches the hope of his family, and kills the son, which could be expected, before he is born. This wickedness is now as severely as is possible condemned by the Spirit, through Moses, that Onan, as it were, through a violent and untimely birth, tore away the seed of his brother out the womb, and as cruel as shamefully has thrown on the earth. Moreover he thus has, as much as was in his power, tried to destroy a part of the human race. When a woman in some way drives away the seed out the womb, through aids, then this is rightly seen as an unforgivable crime. Onan was guilty of a similar crime, by defiling the earth with his seed, so that Tamar would not receive a future inheritor.

See the section on Genesis 2 above for a thorough treatment of the passage.  One additional note of interest here: just as modern day evangelicals view "life" as the time when a sperm meets egg, so too did Calvin view "life" as the seed of a man.  To cast this on the ground was tantamount to abortion in that it was to "destroy part of the human race."

Luther, true to his incisive style, minced no words in condemning an anti-child mentality, as manifested through the exercise of birth control:

> Today you find many people who do not want to have children. Moreover, this callousness and inhuman attitude, which is worse than barbarous, is met with chiefly among the nobility and princes, who often refrain from marriage for this one single reason, that they might have no offspring. . . .  Surely such men deserve that their memory be blotted out from the land of the living. Who is there who would not detest these swinish monsters? But these facts, too, serve to emphasize original sin. Otherwise we would marvel at procreation as the greatest work of God, and as a most outstanding gift we would honor it with the praises it deserves. (Lectures on Genesis: Chapters 1-5, 1536; Luther's Works, vol. 1, 118; commentary on Genesis 2:18)

Note an explicit condemnation of the anti-child mentaility: the failure to marvel at and embrace procreation.

> The rest of the populace is more wicked than even the heathen themselves. For most married people do not desire offspring. Indeed, they turn away from it and consider it better to live without children, because they are poor and do not have the means with which to support a household. . . . But the purpose of marriage is not to have pleasure and to be idle but to procreate and bring up children, to support a household. . . . Those who have no love for children are swine, stocks, and logs unworthy of being called men and women; . . . (Lectures on Genesis: Chapters 26-30; Luther's Works, vol. 5, 325-328; vol. 28, 279; commentary on the birth of Joseph; cf. Luther's Works, vol. 45, 39-40)

Once again, Luther pits the work of having children against attitudes of idleness, pleasure, and a love of little ones.

> But the greatest good in married life, that which makes all suffering and labor worth while, is that God grants offspring and commands that they be brought up to worship and serve him. In all the world this is the noblest and most precious work, . . . Now since we are all duty bound to suffer death, if need be, that we might bring a single soul to God, you can see how rich the estate of marriage is in good works. (The Estate of Marriage, 1522; Luther's Works, vol. 45, 46)

And finally the positive: that the work of rearing children in marriage is to embraced as a high calling.

### The Case For the Permissiveness of (Permanent) Birth Control

With objections to permanent birth control addressed, what might be the positive, biblical reasons for its use?

> Notwithstanding, the Scriptures say nothing definitively about birth control considered as such. Despite the anti-family bias that created the default assumptions of the world around us, we still have to be careful not to go beyond what is written. We especially have to take care not to go beyond what is written. Slavish following of the world is bad, but so is knee-jerk reaction to it.

#### Sex Not Tied to Procreation

I Corinthians 7:2 gives a biblical basis for marital intimacy apart from procreation: 

> But because of the temptation to sexual immorality, each man should have his own wife and each woman her own husband.

The entire book of the Song of Solomon deals with a husband's delight in his wife.  Note that the allegory of Christ and the Church in Song of Solomon *only* works if husbands and wives are to delight in one another in this way.

Entire posts/books have been written on this subject, as it is a major divide between Roman Catholics and Protestants.  A light justification will serve the purpose of addressing the topic at hand.

#### Kingdom Living in a Fallen World

One of the points made by John Piper out of I Corinthians 7 is that Paul gives an admonition to be single.  Here's the interesting part: marriage is given as a "blessing" from God (Proverbs 18:22), and is typically viewed as a normative part of #adulting.  *But*, for the sake of the Kingdom in the specific context of persecution + gospel proclamation under a tyrranical government, Paul says that singleness could not only be tolerated, but *encouraged*.  John Piper says it like this:

> That is amazing. And here is what I infer from it: There are realities that came into the world after the fall that make life more complicated than it was before the fall regarding marriage and having children. If there were no sin, if there were no need for a world mission of sacrifice and martyrdom and suffering — in other words, if we lived before the fall — then everybody marrying and everybody having children would be virtually an absolute. But we don’t live in that world, and other factors determine how we live.

#### Practical Considerations

Recreation, etc.
Mom's temperament, physical toll.

## Conclusion: Live for the Kingdom

In short, my conclusion is that a vasectomy is perfectly acceptable *when done for Kingdom purposes*.  This is a heavy decision, and must be approached with much care, council, and prayer.  I know that my heart is wickedly deceitful (Jeremiah 17:9), and I would seek to make finances, convenience, vacations, or whatever else my idol apart from the work of Christ - and then mask my idolatry with religious-sounding phrases.  As such, I'll be earnestly seeking to weigh my reasoning well, and walk in obedience to my king with this decision!

## References + Resources

 - [APJ: Permanent Birth Control](https://www.desiringgod.org/interviews/is-permanent-birth-control-a-sin) - John Piper.  'nuff said.
 - [11 Theses on Birth Control](https://dougwils.com/books/eleven-theses-on-birth-control.html) - Douglas Wilson.
 - [Family Driven Faith by Voddie Baucham Jr.](https://www.amazon.com/Family-Driven-Faith-Doing-Daughters/dp/1433528126) - While I disagree with much of this book, I've read it in order to understand one of the more prominent fundamentalist positions in today's church.  Voddie takes a hard line for having as many kids as possible, homeschooling, and the like.  It's useful for exposing the way broader societal ideals have infiltrated the church.
 - [Three(ish) Views on Contraception](https://www.reformation21.org/blog/threeish-views-of-contraception) - Broader historical/non protestant views analysis.
 - [Not Completely Terrible BBC Article](https://www.bbc.co.uk/religion/religions/christianity/christianethics/contraception_1.shtml)
