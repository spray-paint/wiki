---
title: Our Wedding Vows
subtitle: Taken August 14, 2010
comments: false
---

## Josh
I Joshua, take you Justine to be my wife before God who brought us together, to have and to hold from this day forward, to love and cherish you, even as Christ loved the church and gave himself for her, to lead you and share all of life’s experiences with you by following God through them, that through his grace, we might grow together into the likeness of Jesus Christ, our savior and lord. I pledge myself and my faithfulness to you.

## Justine
I Justine, take you Joshua to be my husband before God who brought us together, to have and to hold from this day forward, to love you and cherish you, to submit myself unto you, and to follow you through all of Life’s experiences as you follow God, that through his grace, we might grow together into the likeness of Jesus Christ, our savior and lord. I pledge myself and my faithfulness to you.