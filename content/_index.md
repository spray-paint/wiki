## What is This, Anyway?

After many years of keeping binders, notebooks, and journals, I was left with a substantial amount of scattered information.  Study notes, references, and general thoughts lay strewn about in nightstands, shelves, and closets.

This is my attempt at systematically organizing said information.

<div>Icons made by <a href="https://www.flaticon.com/authors/smashicons" title="Smashicons">Smashicons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a> is licensed by <a href="http://creativecommons.org/licenses/by/3.0/" title="Creative Commons BY 3.0" target="_blank">CC 3.0 BY</a></div>
