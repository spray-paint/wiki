---
title: The Mark of the Christian
subtitle: Francis Schaffer
comments: false
---

At only 59 pages, I picked up this book first because I thought it would be a gentle way to easy myself into the study of missiology.  However, while light in page count, this book exposited Jesus' words in a rich and meaningful way that leaves the reader with a healthy dose of heavy truth.

# Summary
The book makes two central points, from John 13 and John 17.  Namely:

 - The world will judge whether or not a Christan's faith is genuine by his love for other Christians.  This is a rare responsibility bequeathed to unbelievers
 - "We cannot expect the world to believe that the Father sent the Son, that Jesus' claims are true, and that Christianity is true, unless the world sees some reality of the oneness of true Christians."

The central theme being the outward display of love as a "mark" delineating true followers of Christ from the rest of the world.  Schaffer is careful to qualify the actions of this love within the bounds of sound doctrine.  However, he pushes the reader to weigh heavily the cost of being unloving - both to an individual's soul and larger communal witness of the saints.

# Questions

Schaffer uses the text from the High Priestly Prayer in John 17 as evidence that the world's belief is contingent on the outwardly displayed oneness of the Church.  However, it was not readily apparent to me from modern translations, take the ESV for instance on John 17:21 :

> that they may all be one, just as you, Father, are in me, and I in you, that they also may be in us, so that the world may believe that you have sent me.

At *first*, it was unclear to me whether the causality of `so that the world may believe` came from the `that they may all be one` clause, or the `that they also may be in us` clause.  Notice how differently those might read: one puts the cause of the world's belief at the church's oneness, the other puts the cause solely on the church's being in Christ.  However, this question (and Shaffer's point) was resolved with further reading on the following verses.  Namely verse 23:

> I in them and you in me, tht they may become perfectly one, so that the world may know that you sent me and loved them even as you loved me.

This verse makes the intention of the church's internal oneness exceedingly clear (and frankly I'm not sure why Shaffer did not include it in his analysis).  It is for the world to know that Christ was sent by God himself, and that the church shares in the Father's love of Christ.

# Reflections

I would be in the camp Shaffer mentions that tends to favor the church's "purity" or doctrinal accuracy as tantamount.  My tendency will be to draw distinctions and fail to exercise love towards brothers who have different convictions about minor issues.  What's more, these distinctions don't just show up doctrinally, they show up practically.  For instance, I was recently in a conversation with a brother who bought a brand new Jeep Gladiator.  Knowing that this particular brother is of normal means, I immediately felt a disconnect: Why would he spend THAT MUCH money that can be used for the kingdom on such a trifle thing?  What lot do I have with someone whose treasure appears to be so radically different?  How do we even fellowship when all he wants to talk about is a new car - something I couldn't care less about (and actually view as a symbol of our society's decay and deterioration)?

However, Schaffer reminds us that our intentionality in directing our love must work in the opposite way I'd normally be inclined:

> As the differences among true Christians get greater, we must consciously love and show a love which has some manifestation the world may see.

His point: the greater disconnect felt between my brother with the Gladiator and I, the more inentional I must be to love him - both inwardly in my attitude and outwardly in my actions.  What a call!  I'm still working out what that looks like practically.  I do believe that pointing the conversation towards spiritual matters would be mutually edifying.  However, I wonder if my inability to relate to the "coolness" of his car might be perceived as uncaring, as if I should wade through a conversation about the car to build relational equity.  Or would I simply be reinforcing its place as a hierarchical symbol and means of communicating one's personality through the medium of a vehicle (once again, a practice I find appalling, nonsensical, and often exploitative).

As you can see, I'm still working through what this love and unity looks like practically.

However, the prevailing "take away" from this book is that the visible demonstration of this love, will be essential for the mission field.  No doubt there would be countless differences like the example above, and my call would be to love well: both internally, and externally.  Without this love, I have no reason to expect an unbelieving world around me to be receptive at all to the gospel of Christ.
