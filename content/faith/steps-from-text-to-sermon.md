---
title: Steps From Biblical Text to Sermon
subtitle: According to Sidney Greidanus
comments: false
---

The following are the steps that Sidney Greidanus lays out in his book [Preaching Christ from the Old Testament](https://www.amazon.com/Preaching-Christ-Old-Testament-Hermeneutical/dp/0802844499/).  I would highly recommend both the book and the approach.

 1. Select a textual unit with an eye to congregational needs.
 1. Read and reread the text in its literary context.
 1. Outline the structure of the text.
 1. Interpret the text in its own historical setting.
 1. Formulate the text's theme and goal.
 1. Understand the message in the contexts of canon and redemptive history.
 1. Formulate the sermon theme and goal.
 1. Select a suitable sermon form.
 1. Prepare the sermon outline.
 1. Write the sermon in oral style.
