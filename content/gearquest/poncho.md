---
title: Poncho
subtitle: Stay dry, look even cooler
section: gearquest
comments: false
---

# Gearquest: Poncho

This quest is around the quintessential piece of rain gear: the Poncho.

Upon completion, adventurers will earn a [children's rain poncho](https://smile.amazon.com/Totes-Royal-Blue-Childrens-Poncho/dp/B005XL3K30).

## Information Card

![Poncho Information Card](/img/gearquest-poncho.png)

## Activities

All activities can be performed with a "loaner" adult poncho until the adventurer earns his or her own poncho.

 - **Get Soaked:** Have the adventurer put on full hiking clothes, then spray him or her down with the hose.  Next, perform a short walk and ask him or her to comment on how their skin feels, weight of clothes, and what the water does for body temperature.
 - **Don't Get Soaked:** This time, have the adventurer put on full hiking clothes, AND a poncho.  Once again, spray with the hose and perform a short walk.  Ask him or her to talk about the differences between the walk with the poncho and without the poncho.
 - **Makeshift Shelter:** Build a makeshift shelter out of the poncho and rope.  Talk about reasons why a makeshift shelter might be necessary.

## Test Questions

 - Should you always carry a poncho when hiking, or just when the weather says there will be rain?
 - Why is it important to keep your clothes dry?
 - Why is it important to keep your skin dry?
 - Besides wearing a poncho, what else can you use it for?

## Non-Standard Use

<iframe width="560" height="315" src="https://www.youtube.com/embed/F4O5Q-9uYas?controls=0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
