---
title: Band-Aid
subtitle: Stop the Bleeding, Keep on Trucking
section: gearquest
date: "2019-10-22"
comments: false
---

# Gearquest: Band-Aid

One of the most simple pieces of first-aid gear, yet one of the most commonly used and essential.

Upon completion, adventurers will earn their very own [first aid kit](https://www.amazon.com/Coghlans-Trek-First-Aid-Kit/dp/B0716LH1WV), stuffed with various sizes of bandaids.  All other gear will be earned on subsequent quests as they learn varius first-aid techniques.

## Information Card

![Band-Aid Card](/img/gearquest-bandaid.png)

## Activities

 - **Pretend Cut:** Take a pen or marker and draw a cut somewhere on your body.  Ask the adventurer to apply first aid to the cut or scrape.  Ensure they follow all of the steps prior to bandaid application.

## Test Questions

 - What should you do before you apply a bandaid?
 - What is there isn't a sink around to wash the cut off?
 - How long should you wait before taking the bandaid off or applying a new one?

