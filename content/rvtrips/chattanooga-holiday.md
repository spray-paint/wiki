---
title: Holiday RV Park in Chattanooga
subtitle: Good ratings, disappointing stay
date: "2019-09-15"
comments: false
---

# Because The Internet Isn't Reality...
When deciding to come to Chattanooga, our family had several different RV parks to choose from.  Being relatively new to this, we chose a 5-star rated, Good Sam Club, bells-and-whistles type RV park with a great web presence.  Well... I guess it goes to show that the internet can be wrong sometimes!

Our destination: [Holiday Travel Park of Chattanooga](http://chattacamp.com/)

## Roads + Travel
We took I-59, I-24, and I-75 to the Holiday RV Park.  The trailer performed well on the highway, and with the exception of getting pushed a bit by the wind, it was a relatively easy drive.  There was a patch of exceptionally rought road above Fort Payne on I-59 where it appears some resurfacing is taking place - let to a jolting, bouncy ride that made the kids a bit carsick.

The residential streets going into the park are not the widest, with a few tight turns.  However, once you're in the park there is plenty of space to maneuver.  As long as you avoid rush hour coming through Chattanooga, the drive is reasonable.

Side note: made our first unplanned/Google Mapp'd stop for an emergency potty break.  Was successful at getting in and out of a gas station because of one great tip from the RV's previous owner: "Always have a plan before you pull in".

## Site Info
First the good: sites are mostly level, almost all pull-through, and come equipped with amenities like cable TV and the semblance of a Wifi signal.  Everything was clean, well-kept, and the staff was friendly.  Our site came equipped with a small cement pavilion, fire pit, and a picnic table.  Amenities we light, but a saltwater pool that went from 3-5 feet, a small fenced playground, and medium-sized field were enough to keep the family busy on slow evenings.  Finally, it was conveniently located to groceries and shopping.

![camper parked in lot](/img/chattanooga-travel-park.jpg)

And now the bad:

 - Wifi: While there was usually indicators of strong signal, it was absolutely unusable.  There were spells in the mornings where there was enough bandwidth available for light browsing, but in the evenings most devices couldn't even maintain a connection without timeouts.
 - Noise: Being in the middle of the city, we could hear the noise of cars passing, sirens, and even the roar of a drag strip on Friday night.  If you're looking to get away from the hustle and bustle, this is *not* the park for you.
 - Proximity: Being listed as a Chattanooga park, we thought it convenient to all attractions - particularly since it was in the middle of a city.  However, we were wrong.  Most of the hiking and outdoor attractions are on the Southwest side of the city, with this park being on the East side.  It was 20 to 30 minutes to most hiking and attractions.

## Things to do
Hiking in Chattanooga proved to be lackluster.  The bluff trail off of Sunset point was poorly maintained and covered in downed trees.  We audibled to the Glenn Falls trail, where we had a much better hike, albeit to a completely dry waterfall.

![overgrown bluff trail](/img/chattanooga-bluff-trail-overgrowth.jpg)

Ruby Falls and touristy things are always available here, but I don't think we'll be back for the hikes.

