---
title: DeSoto 2020
subtitle: So nice, we came back twice.
date: "2020-07-03"
comments: false
---

Since DeSoto State Park has been a family favorite for years (and since we had to cancel other plans due to CoronaVirus), we decided to take another quick jaunt.  See the former post for more information on subjects like roads and travel.

## Site Info
We once again stayed in the upper loop, but this time at a nice corner site with a large "yard".  Site 29 not only boasts shady trees and a large space, but immediate access to the main hiking trail loop in the park.  We were even visited at our site by deer a couple of nights!

![desoto RV site](/img/desoto-2020-site.jpg)

Downsides to this site was its distance from bathouse (and as a result its weak WiFi signal) and zero cell-phone reception.  We were forced to walk to the top of the nearby hill to make a call or send a text.

We scouted some sights on the lower loop (where I gather that WiFi is more reliable anyway), and sites 76-81 on the outer edge of the lower loop seem to have a perfect combination of shade, space, seculusion, and SIGNAL!

## Things to do

We explored more hiking trails this year, and got to see the last few major falls in the park that we had not visited yet.  Trails were well-kept, and easy enough for traversal by toddlers.  The trails offered several bouldering stations and places for kids to climb, jump, and otherwise get active.  The waterfalls were beautiful, but small.  I think the highlight of camping here will continue to be its proximity to swimming and water activities in Little River Canyon.

![desoto waterfall](/img/desoto-2020-waterfall.jpg)

We also decided to try the Screaming Eagle zipline course for the first time.  We were pleased with the professional and friendly staff, and enjoyed our time above the trees.  However, with only 5 bridges and 4 ziplines on the course, the $50 price tag is a little steep.

Kayaking was offered by the park at the top of DeSoto Falls.  It was reasonably priced, but involved a down-and-back trip (instead of one-way) with banks largely surrounded by residential property.  It suffices if you're just looking to hit the water, but can be disappointing if you're hoping to take in scenery like that of surrounding canyon landscapes.
