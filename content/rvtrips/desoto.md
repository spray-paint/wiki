---
title: RV Trip DeSoto State Park
subtitle: Great spot for a first-timer
date: "2019-07-21"
comments: false
---

# Our First RV Trip!
This was the maiden voyage for our RV, and the very first RV trip we had ever taken as a family.  We chose the spot for a few reasons:

 - We had been camping at this park for several years, and were familiar with it.
 - We knew the drive was short, and the roads were good.  No highways!
 - Campsites were large and very accessible.

## Roads + Travel
Roads were well-maintained overall.  We were able to use back-roads the entire way, but if you're coming in on I-59 you'll have to navigate a number of difficult turns in downtown Fort Payne.  Entry to the park itself can also get a bit hairy, with winding roads - sometimes lined with tall boulders.  Could spell trouble if two rigs were to try to pass simultaneously.  Thankfully, we didn't encounter another one on those small stretches!

The camp store, where you check-in, provides a really large parking pad for RV's across the street.  Has space for several, and even during peak Friday check-in it did not fill up.

![parking pad with RV](/img/desoto-checkin.jpg)

## Site Info
Sites were very large and accommodating, with good privacy.  We could see and hear several of our neighbors, but there was plenty of shade and woods to help us feel like we were away from it all.

The site had a significant front/back slope, requiring block or stabilizer jacks.  This trend was common amongst other camp sites as well, with some rigs building small towers to overcome the significant slopes.  However, the site was very level from side-to-side.

![desoto RV site](/img/desoto-site.jpg)

Full hookups were available and worked flawlessly.  Paired with the the campfire ring and picnic table, the site was very nicely equipped.  The one significan issue that plagued our stay, however, was NO WIFI.  Our family stayed in the "upper" loop, which recieves its wifi signal from the centrally located bathouse.  We picked a site fairly close, yet were unable to pick up any signal whatsoever.  After several trips to and from the country store, we were met with a general "yeah... sometimes it works, sometimes it doesn't" attitude from the employees.

Rumor has it that wifi is better on the lower loop, but if it's essential you probably shouldn't count on it being available at this park.  Seems flaky at best, and staff doesn't really seem to care about fixing it quickly.

## Things to do

DeSoto has great hiking that ranges from a short boardwalk jaunt to canyon trails that span far beyond the reach of day hikes.  Highlights include Indian falls and an old CCC quarry trail.

Additionally, it's a short (15 min) drive to the swimming area of Little River Canyon.  This provides a great spot for even the smallest of kids to go swimming, as there are a number of pools along the top of the waterfall that flow gently and remain shallow.

