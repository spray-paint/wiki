---
title: KOA in Cherokee, North Carolina
subtitle: Resort camping in the Smokies
date: "2019-10-23"
comments: false
---

# Nothing like the Smokies in the Fall!

## Roads + Travel
200+ miles, but a straight shot with no interstate - this was by far the longest haul we've ever done.  However, US-411 and a number of other 4-lane highways made for a surprisingly smooth drive.  Roads got a bit curvy around the Nantahala and Blue Ridge, but never so much that it was a problem.  There was always a steady stream of 18-wheelers and box trucks opposite us to provide reassurance that the roads ahead would be friendly to our rig.

The only difficulties we encountered were a somewhat tight roundabout just outside of Cherokee, as well as a hairpin turn on a mountain ridge a couple of miles from the campground.  Additionally, Google Maps took us to the wrong side of the river - opposite of KOA.  It led to an unaticipated 3-point turn in a makeshift driveway.  On future trips, remember not to trust the GPS!

![camper at KOA pad](/img/cherokee-wrong-side-of-river.jpg)

Made our first successful fuel stop close to 2 hours in.  Planned the route, swung out wide, and was able to successfully navigate a standard Speedway.  While it's still a bit unnerving, I think I'm starting to get the hang of gas stations with an RV.  However, I've encountered no small number lately in which I've thought "Wow, I'd never get a camper in and out of here!".

## Site Info

Our "deluxe" back-in site was located along the back fence, which butts up against one of the stocked trout ponds and a moderately trafficked road.  The view was not terrible, but the constant traffic could definitely be heard for the majority of the day and night.  The pad was very large, with an oddly placed picnic table and fireplace towards the front.  We pulled the camper so as to center our awning on what we thought to be the patio area, which led to **LONG** runs for all of our connections.  Had to buy a 10ft extension (at the overpriced KOA store rate) for the sewer hose, and upgrade cable to a 50 ft. run.

![camper at KOA pad](/img/cherokee-koa-site.jpg)

The check-in process was not without issue.  We were alotted a site, only to be told that site was taken.  After some minutes of back-and-forth, all the confusion was cleared up and we were one our way - with a couple of free bundles of (only somewhat wet) firewood for our trouble.  There is a parking section of diagonal spaces just past the main store/check-in desk - all of which were full on the Friday afternoon we arrived.  We made due, shimmying in between a huge rig and a small Honda, but it was tight.

Overall, the campground was well-kept.  It definitely got crowded during the weekend, but was surprisingly empty during the week.  Often times we would be the only family at the jump pillow or in the pool.  Roads were good overall, but the attention seemed to be mainly towards the front of the campground, tending primarily to the pull-through and patio'd sites.  For instance, this massive puddle was a few sites down:

![KOA puddle](/img/cherokee-koa-puddle.jpg)

## Things to do

Cherokee is as close as you can get to the South side of the park - we were just across the river from the Oconoluftee Visitor's center.  This means it provides amazing access to Great Smoky Mountains National Park.  For instance, this is a creek near the Kephardt shelter on the Southern side of the park:

![Creek at Kephardt Prong](/img/cherokee-creek.jpg)

It was still a considerable distance to the heart of Great Smoky Mountains National Park, as the park's size puts any accommodation in surrounding cities an arms length away.  Clingman's Dome and Newfound Gap were located 15+ miles from the park entrance and could take up to an hour to reach, depending on what kind of traffic you got behind.  However, the views from Clingman's Dome as the clouds rolled in over the mountains at sunset were well worth the drive and the crowds.

![Sunset at Clingmans Dome](/img/cherokee-clingman.jpg)

Aside from hiking, there is a rich Cherokee experience in the city, where you can visit the museum, and indian village, or the long-running "Unto These Hills" show.  The city is fairly light on dining options, with neighborhood eateries like Wise Guys Grill and Pizzaria offering a small-town dining experience.  Visitors will have to make the drive to Bryson City to get into a more touristy sit-down restaurant scene.

There is a small downtown shopping area that has a Gatlinburg-like feel, but with an Indian flair.  Lots of shops offering discounts, t-shirts, and made-in-China trinkets.  There's a predictable smattering of fudge shops, "outfitters", and botiques along the way, with some genuinely interesting purveyors of true Cherokee cultural artifacts.  If you're downtown with kids, there's an amazing park, water-ups, and wading pools with rocks.  It's a great way to spend a warm afternoon.
