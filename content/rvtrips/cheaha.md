---
title: Cheaha State Park
subtitle: Weekend Getaway with No Kids
date: "2020-05-17"
comments: false
---

# Just The Two of Us
While we generally enjoy RV'ing as a family, my wife and I decided the make our first trip of the 2020 camping season ourselves.  This gave us some much needed time away (in the middle of COVID) and provided a stree-free way to de-winterize the camper (and work on anything that needs fixing).

## Roads + Travel
Roads were all very well maintained and provided sufficient room for the numerous RVs and 18-wheelers we passed along the way.  They were mountain roads, and involved steep grades and slow, winding turns.  However, the grades are manageable and there are no hair-pin curves involved.  Overall, while the roads weren't the easiest, their well-kept nature made it reasonable with a vehicle in tow.

The check in process was both painful and frustrating.  A broken-down car curtailed the much-needed space for a u-turn in the camp store parking lot, while a park ranger's truck proceeded to block the roadside pull-up parking spot.  We arrived, checked in, and were assigned a site where we were surprised to find another RV had taken up occupancy.  After knocking on the door of the unoccupied staff house, the folks at check-in were surprised to learn that the space was still occupied at 3pm, after a supposed 11am check-out.  It took 2 more trips before we were finally reassigned to another campsite.  Suffice it to say that check-in was a frustrating experience and we have no evidence that the staff perform basic functions like validating campsites are open.

## Site Info
We FINALLY made it to our site after a couple hours of "check-in" and waiting.  We stayed in the upper campground, where all sites have full hook-ups and roads are both paved and wide.  What's more, most of the sites are pull-through and offer sufficient privacy and space between neighbors.  We stayed in 27B, but have our eyes on 28 or 27 for the next trip.  Site 28 faces the woods, has its own night light, and a front-yard-like feel to its ajdacent woods.

![cheaha RV site](/img/cheaha-site.jpg)

The campground was pretty, with a large amount of shade and mature trees.  Each site had a nice fire ring and a (HEAVY) metal picnic table.  However, be prepared for 0 cell phone service or Wifi (AT&T).  To communicate, we had to drive towards the park entrance and would find service somewhere along the way.  There is no advertised wifi available at the campsite, just a weak signal if you drive to the park store.  Bottom line: bring some DVDs if you've got antsy kids on a rainy day.  Otherwise, enjoy being disconnected.

![cheaha upper campground](/img/cheaha-campground.jpg)

## Things to do

Short trails abound inside the park, with longer trails available via the Pinhoti or closeby wilderness (I.E. High Falls/Adams Gap).  We hiked pulpit rock for the first time, and were really pleased with the trail and the views.  It's a great short hike for the family!  

![view from Pupit Rock](/img/cheaha-pulpitRock.jpg)

Other than the restaurant inside the park, it's going to be a significant (30+ min drive) to any shopping, groceries, or restaurants - so plan accordingly.  What's more, the only playground available is down by the lake, so there are limited non-hiking activities for kids.  
