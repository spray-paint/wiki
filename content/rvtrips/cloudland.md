---
title: Cloudland Canyon
subtitle: Fall weather, green leaves
date: "2020-10-05"
comments: false
---

Cloudland Caynon is yet another family favorite.  While we've been there multiple times, staying in tents and yurts, we were excited to try out the park experience in an RV.

## Roads + Travel
While no highway driving was necessary, there were a number of steep inclines and windy hills en route.  The camper next to us had actually blown out a tire on the way up, and lost a good bit of the platic wheel well/cover to the tire malfunction.  Just make sure the brakes and tires are in good working order, and your load is well balanced!

## Site Info
We stayed at site 13 in the West Rim campground.  The park has two sets of facilties: the East side of the Canyon has a small, somewhat cramped RV campground close to main trailheads and cabins.  The West side of the canyon has a larger, wooded, and more spacious RV camp along with Yurts and primitive tent sites.  While it's a bit of a drive to get to some of the primary trailheads, I'd highly recommend the West Rim for its privacy an wooded setting.

![Cloudland RV](/img/cloudland-site.jpg)

The West Rim also provides access to all the trails, and a number of sites even have paths cut - your own private trail access point.  Just be careful getting in!  The RV campsite loop is narrow and there are a number of sharp turns that lend themselves to putting a tire or two in the ditch!

This was our first experience without sewage hookups.  Since we were only there for a few days, we decided to make the bath houses our first choice, while sparingly using the RV toilet and sink.  Thankfully, we made it the weekend without having to hook up and make a trip to the dump station.  However, I will say that having to constantly keep an eye on the tanks was less than ideal.  However, everything worked out and the dump station on the way out was a relatively uneventful experience.

## Things to do

If you're looking to be outside, Cloudland is the place to be.  The West Rim campground has an excellent playground in the center of the loop, where we passed many mornings before breakfast and evenings waiting on dinner.  The camp store is reasonably stocked, and offers some fashionable additions (Hammocks, Kavu purses, etc.) to common staples.

Hiking trails are very well maintained, and offer variety.  Take the rim loop for the views, or descend hundreds of stairs to one of the waterfalls in the canyon.  It's hard to go wrong with the trail system here.

![Cloudland Canyon](/img/cloudland-canyon.jpg)

We didn't venture out to local towns, as we were only staying for a short time in the midst of a pandemic.  It does not appear to have a large restaurant or shopping scene nearby, as the park is somewhat remote.
